<?php
/**
 * @file
 * Devel module integration.
 */

/**
 * Implements hook_btsync_menu_alter() on behalf of devel.module.
 */
function devel_btsync_menu_alter(&$items) {
  $items['admin/config/services/btsync/testing'] = array(
    'title' => 'Testing',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('btsync_testing_form'),
    'access arguments' => array('administer btsync'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'btsync.admin.inc',
  );
}

/**
 * Testing form callback.
 */
function btsync_testing_form($form, $form_state) {
  $form['methods'] = array(
    '#title' => t('Method'),
    '#type' => 'select',
    '#options' => array(),
    '#ajax' => array(
      'callback' => 'btsync_testing_form_ajax',
      'wrapper' => 'method-ajax-wrapper',
    ),
  );
  $methods = btsync_methods_get_all();
  foreach ($methods as $method) {
    $form['methods']['#options'][$method->method] = $method->name;
  }
  asort($form['methods']['#options']);
  $form['methods']['#default_value'] = isset($form_state['values']['methods'])
    ? $form_state['values']['methods']
    : key($form['methods']['#options']);

  // Method parameters.
  $method = $methods[$form['methods']['#default_value']];

  $form['method'] = array(
    '#title' => 'Method parameters',
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#prefix' => '<div id="method-ajax-wrapper">',
    '#suffix' => '</div>',
    '#description' => $method->description,
  );
  $parameters = $method->arguments;
  if (count($parameters) > 0) {
    foreach ($parameters as $key => $parameter) {
      $parameter += array(
        'description' => '',
        'type' => 'text',
        'default value' => '',
        'required' => FALSE,
      );

      $form['method'][$key] = btsync_parameter_to_fapi($key, $parameter);
      $form['method'][$key]['#default_value'] = isset($form_state['values']['method'][$key])
        ? $form_state['values']['method'][$key]
        : $form['method'][$key]['#default_value'];
    }
  }
  else {
    $form['method']['#description'] = !empty($form['method']['#description'])
      ? $form['method']['#description'] . '<p>' . t('This method has no parameters.') . '</p>'
      : t('This method has no parameters.');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Test'),
  );

  return $form;
}

/**
 * Testing form ajax callback.
 */
function btsync_testing_form_ajax($form, $form_state) {
  return $form['method'];
}

/**
 * Testing form submit callback.
 */
function btsync_testing_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  $method = btsync_methods_get_method($form_state['values']['methods']);

  $parameters = $method->arguments;
  $data = array();
  if (!empty($parameters)) {
    foreach (array_keys($parameters) as $parameter) {
      if (isset($form_state['values']['method'][$parameter])) {
        $data[$parameter] = $form_state['values']['method'][$parameter];
        if ((!isset($parameters[$parameter]['required']) || $parameters[$parameter]['required'] == FALSE) && (!isset($parameters[$parameter]['type']) || 'boolean' != $parameters[$parameter]['type']) && empty($data[$parameter])) {
          unset($data[$parameter]);
        }
      }
    }
  }

  $response = btsync_method_callback($method->method, $data);
  dpm($response, t('Response'));
}

